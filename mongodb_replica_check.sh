#!/usr/bin/env bash
DB_HOST=
DB_PORT=
DB_USERNAME=
DB_PASSWORD=
MONGO=`which mongo`
JQ=`which jq`
EXIT_ERROR=1
EXIT_OK=0

if [ ! -x "$MONGO" ] ; then
  echo "mongo not found"
  exit $EXIT_ERROR
elif [ ! -x "$JQ" ] ; then
  echo "jq not found"
  exit $EXIT_ERROR
elif [ $# -eq 0 ] ; then
  echo "No values pass"
  exit $EXIT_ERROR
fi

MONGO_CMD="sudo $MONGO --host ${DB_HOST:-localhost} --port ${DB_PORT:-27017} --authenticationDatabase admin"
[[ "$DB_USERNAME" ]] && MONGO_CMD="${MONGO_CMD} --username ${DB_USERNAME}"
[[ "$DB_PASSWORD" ]] && MONGO_CMD="${MONGO_CMD} --password ${DB_PASSWORD}"

replica_member_status() {
replica_member_status_output=$(
        $MONGO_CMD <<< "db.adminCommand( { replSetGetStatus : 1 } )" |\
        sed -e '1,3d' |\
        sed -e 's/NumberLong(\(.*\))/\1/
          s/ISODate(\(.*\))/\1/
          s/BinData(\(.*\))/1/
          s/ObjectId(\(.*\))/\1/
          s/Timestamp(.*)/"&"/
          s/^bye//
          s/"\([0-9]*\)"/\1/' |\
       jq .members[].health)

mongo_status=${PIPESTATUS[0]}
if [ $mongo_status -ne $EXIT_OK ] ; then
  echo "mongo exec error"
  exit $EXIT_ERROR
fi

jq_status=$?

if [[ $replica_member_status_output = *"0"* ]]; then
    echo "unhealthy"
else
    echo "healthy"
fi
}

slave_lag() {
slave_lag_output=$( $MONGO_CMD <<< "rs.printSlaveReplicationInfo()" |egrep " behind the primary" |awk '{print $1}')

if
  [[ -z $slave_lag_output ]]; then
  echo "return null. check user - pass variables"
  exit $EXIT_ERROR
fi

count_replicas="$(echo $slave_lag_output |sed -e "s/ /|/g")"
char="|"
replica_num=$(awk -F"${char}" '{print NF-1}' <<< "${count_replicas}")

if [[ $replica_num -eq 0 ]]; then
    echo $slave_lag_output
else
    sum_replicas_lag=$(echo $slave_lag_output |sed -e "s/ / + /g")
    total_lag=$(($sum_replicas_lag))
    echo $total_lag
fi

}

case "$1" in
    'replica_member_status')
    replica_member_status
;;
    'slave_lag')
    slave_lag
;;
esac

