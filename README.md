# Zabbix MongoDB Check Plugin

 This is based on Netkiller[1] Mongo template which includes Zabbix version
 3.4 improvements and additional replica set checks.

## Configuration

1) Import the template file to zabbix server.

2) Place the mongodb_check.sh (and if you have replicaset setup also
mongodb_replica_check.sh) to /usr/local/bin/ directory on the mongodb
instances and give them exacutable permisson.

3) If your mongodb instances require authentication, set the DB_USERNAME
and DB_PASSWORD variable in mongodb_check.sh 
and mongodb_replica_check.sh file. Otherwise leave them blank.

4) Set sudo access to /bin/mongo, /bin/mongostat for zabbix user like
below:

    ```
    Cmnd_Alias MONGO = /bin/mongo, /bin/mongostat
    ...

    zabbix	ALL=(root)	NOPASSWD: MONGO
    ```

5) Place the userparameter_mongodb.conf file to /etc/zabbix/zabbix_agent.d/ dir
on the mongo instances.

7) Install jq to mongodb instances.

6) Restart zabbix_agent service.


Note that, mongodb_replica_check.sh slave lag function returns total sum
of the all secondary nodes slave lag, so if you use delayed replica node
you need to take this into account.

[1] https://github.com/oscm/zabbix/tree/master/mongodb
